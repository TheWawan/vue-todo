const express = require('express')
const app = express()

const port = 8000

app.use(
  '/client',
  express.static(__dirname + '/public')
)

app.use(
  '/static',
  express.static(__dirname + '/static')
)

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html')
})

app.listen(port, function () {
  console.log(`Example app listening on ${port}`)
})
