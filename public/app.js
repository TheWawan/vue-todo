

var app = new Vue({
  el: '#app',
  data: {
    todoList: [],
    todo: ''
    ,
    oldTodo: ''
  },
  mounted () {
    this.get()
  },
  methods: {
    create() {
      let todo = {
          text: this.todo,
          date: moment()
        }
        this.todoList.push(todo)
        this.send('post', todo)
    },
    destroy(todo){
      axios.post('http://localhost:8080/todo/delete', todo)
          .then((res) => {
            this.todoList = res.data
          })
    },
    edit(todo) {
      this.destroy(todo)
      this.todo = todo.text
      this.$refs['input'].focus()
    },
    send(method ,todo) {
      console.log(todo)
      axios[method]('http://localhost:8080/todo', todo)
      this.todo = undefined
    },
    get(){
      axios.get('http://localhost:8080/todo')
          .then((res) => {
            res.data.map((todo) => {
              this.todoList.push({
                text: todo.text,
                date: moment(todo.date)
              })
            })
            this.todoList = res.data
          })
          .catch((err) => {
            console.log(err)
          })
    }
  }
})
